<?php
if (\Cake\Core\Configure::read('SESSION_DEBUG')) {
    $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf('%s%s',
        $this->getRequest()->getSession()->read('SESSION_DEBUG'), 'SkeletonDefaultLayout;'));
}
?>

<?php $this->extend('DefaultSkeleton.default') ?>

<?php if (filter_var(getenv('PRINT_DEBUG'), FILTER_VALIDATE_BOOLEAN)) { ?>
    <h3>Skeleton default layout</h3>
<?php } ?>

<?php
use PackageLoader\PackageLoader;

$npm = new PackageLoader(\Cake\Routing\Router::url('/') . 'cakephp-skeleton-page/node_modules/',
    ROOT . DS . 'vendor/infotechnohelp/cakephp-skeleton-page/config/npm-map');

echo $npm->map()->stylesheets();
echo $npm->map()->scripts();
?>

    <script>
        $(function () {
            $('.carousel').carousel();
        });
    </script>

<?= $this->fetch('content') ?>