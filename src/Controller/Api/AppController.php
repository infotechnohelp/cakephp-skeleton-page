<?php

declare(strict_types=1);

namespace Skeleton\Controller\Api;

use DefaultSkeleton\Controller\Api\AppController as BaseController;

/**
 * Class AppController
 * @package Skeleton\Controller\Api
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
