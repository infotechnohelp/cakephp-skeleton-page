<?php

namespace Skeleton\Controller\Api;

use Cake\ORM\TableRegistry;
use Skeleton\Exceptions\MyPluginException;

/**
 * Class MyPluginController
 * @package Skeleton\Controller\Api
 */
class MyPluginController extends AppController
{
    /**
     * @return \Cake\Http\Response
     */
    public function getById()
    {
        return $this->response->withStringBody(
            json_encode(TableRegistry::get('MyPlugins')->get($this->request->getData('id')))
        );
    }

    /**
     * @throws \Skeleton\Exceptions\MyPluginException
     */
    public function exception()
    {
        throw new MyPluginException('Exception');
    }
}
