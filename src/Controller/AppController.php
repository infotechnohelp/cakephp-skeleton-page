<?php

declare(strict_types=1);

namespace Skeleton\Controller;

use Cake\Core\Configure;
use DefaultSkeleton\Controller\AppController as BaseController;

/**
 * Class AppController
 * @package Skeleton\Controller
 */
class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();

        if (Configure::read('PRINT_DEBUG')) {
            debug('SkeletonController');
        }

        if (Configure::read('SESSION_DEBUG')) {
            $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf(
                '%s%s',
                $this->getRequest()->getSession()->read('SESSION_DEBUG'),
                'SkeletonController;'
            ));
        }
    }
}
