<?php

declare(strict_types=1);

namespace Skeleton\Lib;

class CssGenerator
{

    /** @todo Create in back-end later (AppController) ??? */
    public static function colorBlinkAnimations(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('color-blink-animations', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateColorBlinkAnimations(array $options)
    {
        $result = [];

        $browserTypes = ['webkit', 'moz', 'ms', 'o'];

        foreach ($options as $animationOption) {
            list($color1, $color2) = $animationOption;

            foreach ($browserTypes as $browserType) {
                $result[] = sprintf('@-%s-keyframes color-change_%s-%s {%s{color: %s;}%s{color: %s;}%s{color: %s;}}',
                    $browserType, $color1, $color2, '0%', $color1, '50%', $color2, '100%', $color1);
            }

            $result[] = sprintf('@keyframes color-change_%s-%s {%s{color: %s;}%s{color: %s;}%s{color: %s;}}',
                $color1, $color2, '0%', $color1, '50%', $color2, '100%', $color1);
        }

        return implode("\n", $result);
    }

    public static function relativeWidthColumnMediaQueries(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('column-media-queries', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateRelativeWidthColumnMediaQueries(array $options)
    {
        list($percentages, $columnMinWidth) = $options;

        $result = [];

        foreach ($percentages as $percentage) {
            $maxWidth = 100 / $percentage * $columnMinWidth - 1;

            $result[] = sprintf('@media screen and (max-width: %spx){
            .column.w%sp {width: %s%s;}.column.w%sp.or-center{text-align:center;}
            }',
                $maxWidth, $percentage, 100, '%', $percentage);
        }

        return implode("\n", $result);
    }

    public static function relativeWidths(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('relative-widths', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateRelativeWidths(array $options)
    {
        $result = [];

        foreach ($options as $percentage) {
            $result[] = sprintf('.w%sp{width:%s%s;}', $percentage, $percentage, '%');
        }

        return implode("\n", $result);
    }

    public static function fontSizes(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('rem-fonts', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateFontSizes(array $options)
    {
        $result = [];

        foreach ($options as $emSize) {
            $result[] = sprintf('.font%s{font-size:%srem;}', $emSize, $emSize / 100);
        }

        return implode("\n", $result);
    }

    public static function zIndex(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('z-index', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateZindex(array $options)
    {
        $result = [];

        foreach ($options as $layer) {
            $result[] = sprintf('.z%s {z-index:%s;}',
                $layer, $layer);
        }

        return implode("\n", $result);
    }

    public static function baseMediaQueries(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('base-media-queries', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateBaseMediaQueries(array $options)
    {
        list($noWrapMaxWidth) = $options;

        return sprintf("@media screen and (max-width: %spx) {.nowrap {white-space: normal;}}", $noWrapMaxWidth);
    }

    public static function menuAndLanguages(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        list($isMobile) = $cssOptions;
        return self::generateCssFile('menu-and-languages', $cssOptions, $title, $htmlHelper, $forceCreate, $isMobile);
    }

    private static function generateMenuAndLanguages(array $options)
    {
        list($isMobile, $menuHeight, $languagesChangedPositionScreenWidthFirst, $languagesChangedPositionScreenWidthSecond)
            = $options;

        $result = sprintf("#menu {height: %s;}", $menuHeight);

        $result .= sprintf(".languages.second {top: %spx;}.languages.third {top: %spx;}",
            $isMobile ? 75 : 40, $isMobile ? 150 : 80);


        if (!$isMobile) {
            $result .= sprintf("@media screen and (max-width: %spx) {
                .languages.first {top: 30px;}
                .languages.second {top: %spx;}
                .languages.third {top: %spx;}
            }",
                $languagesChangedPositionScreenWidthFirst, 70, 110);
        }


        $result .= sprintf("@media screen and (max-width: %spx) {
            .languages.first {top: auto;bottom: 30px;}
            .languages.second {top: auto;bottom: %spx;}
            .languages.third {top: auto;bottom: %spx;}
        }",
            $languagesChangedPositionScreenWidthSecond, $isMobile ? 105 : 70, $isMobile ? 180 : 110);

        return $result;
    }

    public static function label(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        list($isMobile) = $cssOptions;
        return self::generateCssFile('label', $cssOptions, $title, $htmlHelper, $forceCreate, $isMobile);
    }

    private static function generateLabel(array $options)
    {
        list ($isMobile, $menuHeight, $labelChangedPositionWidth) = $options;

        $result = sprintf("#label {height: %s;padding-left: %spx;%s}",
            $menuHeight, $isMobile ? 65 : 0, $isMobile ? "font-size: 2rem;padding-top:15px;" : null);

        if (!$isMobile) {
            $result .= sprintf("@media screen and (max-width: %spx) {#label {height: 20px;padding-top: %spx;}}",
                $labelChangedPositionWidth, ((int)$menuHeight + 10)
            );
        }

        return $result;
    }

    public static function linkAndHelp(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        list($isMobile) = $cssOptions;
        return self::generateCssFile('link-and-help', $cssOptions, $title, $htmlHelper, $forceCreate, $isMobile);
    }

    private static function generateLinkAndHelp(array $options)
    {
        list($isMobile) = $options;

        $result = sprintf(".help {cursor: help;%s}",
            $isMobile ?
                'text-decoration: underline;text-underline-position: under;' :
                'box-shadow: inset 0 0 0 white, inset 0 -1px 0 black');

        $result .= sprintf(".help:hover {color: white; 
        %s
        text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
        animation-name: color-change_white-lightgrey;
        animation-duration: 1s;
        animation-iteration-count: infinite;
        animation-direction: normal;}", $isMobile ? null : 'box-shadow: 0 0 0;');

        $result .= sprintf(".help.darkblue {color: darkblue;%s}",
            $isMobile ?
                'text-decoration: underline;text-underline-position: under;' :
                'box-shadow: inset 0 0 0 white, inset 0 -1px 0 darkblue;');

        $result .= ".help.darkblue:hover {color: lightblue;box-shadow: 0 0 0;animation-name: color-change_lightblue-blue;}";

        $result .= sprintf(".link {color: blue;%s}",
            $isMobile ?
                'text-decoration: underline;text-underline-position: under;' :
                'box-shadow: inset 0 0 0 white, inset 0 -1px 0 blue;');

        $result .= sprintf(".link:hover {color: purple;%s}",
            $isMobile ? null : 'box-shadow: inset 0 0 0 white, inset 0 -1px 0 purple;');

        $result .= ".link > a {text-decoration: none;color: inherit;white-space: nowrap;}";

        return $result;
    }


    public static function base(array $cssOptions, array $generatorOptions)
    {
        list($title, $htmlHelper, $forceCreate) = $generatorOptions;
        return self::generateCssFile('base', $cssOptions, $title, $htmlHelper, $forceCreate);
    }

    private static function generateBase(array $options)
    {
        list($padding, $fontSize) = $options;

        return sprintf(".row {padding: 0 %spx 0 %spx;}
.column {padding: 0 %spx 0 %spx;font-size: %spx;}", $padding, $padding, $padding, $padding, $fontSize);
    }

    private static function generateCssFile(
        string $type,
        array $cssOptions,
        string $title,
        \Cake\View\Helper $htmlHelper,
        bool $forceCreate = false,
        bool $isMobile = false)
    {
        $sharedFiles = ['base', 'label', 'menu-and-languages', 'link-and-help', 'color-blink-animations'];

        $path = (in_array($type, $sharedFiles, true)) ?
            sprintf("%s/webroot/css/generated/global/%s%s.css", ROOT, $isMobile ? 'mobile/' : null, $type) :
            sprintf("%s/webroot/css/generated/page-specific/%s/%s.css", ROOT, $type, $title);

        $helperPath = (in_array($type, $sharedFiles, true)) ?
            sprintf("generated/global/%s%s",$isMobile ? 'mobile/' : null, $type) :
            sprintf("generated/page-specific/%s/%s", $type, $title);

        $file = new \Cake\Filesystem\File($path);

        if (!$forceCreate && $file->exists()) {
            return $htmlHelper->css($helperPath);
        }

        $file = new \Cake\Filesystem\File($path, true);

        $result = null;

        switch ($type) {
            case 'base':
                $result = self::generateBase($cssOptions);
                break;
            case 'base-media-queries':
                $result = self::generateBaseMediaQueries($cssOptions);
                break;
            case 'label':
                $result = self::generateLabel($cssOptions);
                break;
            case 'menu-and-languages':
                $result = self::generateMenuAndLanguages($cssOptions);
                break;
            case 'link-and-help':
                $result = self::generateLinkAndHelp($cssOptions);
                break;
            case 'z-index':
                $result = self::generateZindex($cssOptions);
                break;
            case 'rem-fonts':
                $result = self::generateFontSizes($cssOptions);
                break;
            case 'relative-widths':
                $result = self::generateRelativeWidths($cssOptions);
                break;
            case 'column-media-queries':
                $result = self::generateRelativeWidthColumnMediaQueries($cssOptions);
                break;
            case 'color-blink-animations':
                $result = self::generateColorBlinkAnimations($cssOptions);
                break;
            default:
                throw new Exception('CSS file type cannot be found');
        }

        $file->write($result);

        return $htmlHelper->css($helperPath);
    }

}