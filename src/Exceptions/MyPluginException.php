<?php

namespace Skeleton\Exceptions;

/**
 * Class MyPluginException
 * @package Skeleton\Exceptions
 */
class MyPluginException extends \Exception
{
    /**
     * PathCellValidationException constructor.
     *
     * @param string $message
     */
    public function __construct($message = "")
    {
        parent::__construct($message);
    }
}
