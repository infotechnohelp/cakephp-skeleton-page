jQuery(function () {

    var menuIconWidth = parseInt(jQuery('#menu-icon').css('width'));

    jQuery(window).scroll(function () {
        if (jQuery(window).scrollTop() === 0) {
            jQuery('#menu-icon').css({left: '0px', top: '0px'});
            jQuery('#menu-icon > img').css({
                'border-radius': '0px',
                width: menuIconWidth + 'px',
                height: menuIconWidth + 'px'
            });
        } else {
            jQuery('#menu-icon').css({left: '15px', top: '15px'});
            jQuery('#menu-icon > img').css({
                'border-radius': '50%',
                width: Math.round(menuIconWidth * 1.5) + 'px',
                height: Math.round(menuIconWidth * 1.5) + 'px'
            });
        }
    });
});