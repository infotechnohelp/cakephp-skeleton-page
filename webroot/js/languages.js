jQuery(function () {
    jQuery(".languages.first").click(function () {
        jQuery(".languages.second, .languages.third").animate({'width': 'toggle'}, function () {
            jQuery(this).children('button').children('img').fadeToggle();
        });
    });

    jQuery(".languages").click(function () {
        var lang = jQuery(this).children('button').attr('data-lang');

        if (lang !== 'default') {

            var url = null;

            if (new Helper().getQueryParam('lang') === null) {
                url = window.location.href;

                if (url.indexOf('?') !== -1) {
                    url += '&lang=' + lang;
                } else {
                    url += '?lang=' + lang;
                }
            } else {
                url = window.location.href;
                url = url.replace("lang=" + new Helper().getQueryParam('lang'), "lang=" + lang);
            }

            window.location.href = url;
        }
    });
});