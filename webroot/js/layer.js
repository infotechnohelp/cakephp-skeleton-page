jQuery(function () {

    jQuery('#menu-icon').click(function () {
        jQuery('#layer').show();
        jQuery('#layer-menu').slideDown(500);
        jQuery('body').css('overflow', 'hidden');
    });

    jQuery('.help').click(function () {
        jQuery('#layer').show();
        jQuery('#layer-text').text(help.default);
        jQuery('#layer-text').text(help[jQuery(this).text()]);
        jQuery('#layer-text').fadeIn(500);
        jQuery('body').css('overflow', 'hidden');
    });

    jQuery('#close-layer, #layer').click(function () {
        jQuery('#close-layer').rotate();
        jQuery('#layer').slideUp(1500);
        jQuery('#layer-menu, #layer-text').fadeOut(1500);
        jQuery('body').css('overflow', 'auto');
    });
});